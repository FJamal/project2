<?php
	/*the file the ajax in map calls */
	
	//configuration
	require("../includes/config.php");
	
	//setting up header tell browser what type of response the file cr8s
	header("Content-type: application/json");
	
	$sql = "SELECT stations, name, lat, longi FROM stations JOIN {$_GET["route"]} 
			WHERE stations.abbr = {$_GET["route"]}.stations";
			
	//quering db
	$results = $dbh->query($sql);		
	
	// to store all data of particular route	
	$data = [];
	
	//iterating over the result
	foreach($results as $result)
	{
		$data [] = [
			"stations" => $result["stations"],
			"longitude" => $result["longi"],
			"latitude" => $result["lat"],
			"name" => $result["name"]
			];
	}
	
	//calculation for determining the color for polylines
	//converting GET value like route1 to ROUTE 1 that might b needed for ajax
	$routeid = $_GET["route"];
	
	//inserting a space in bw word and number of "route1"
	$routeid = substr_replace($routeid, " ", 5, 0);
	
	//to uppercase
	$routeid = strtoupper($routeid);
	
	
	//query to get color from db
	$colors = $dbh->query("SELECT * FROM `colors` WHERE 1");
	
	//adding the color that coresponds to route id
	foreach($colors as $color)
	{
		if($color["routename"] == $_GET["route"])
		{
			array_push($data, ["polylinecolor" => $color["color"]]);
		}
	}
	
	//output json
	print(json_encode($data));		
?>
