<?php
	//configuration
	require("../includes/config.php");
	
	$url = "https://api.bart.gov/api/etd.aspx?cmd=etd&orig={$_GET["station"]}&key=MW9S-E7SL-26DU-VV8V";
	
	header("Content-type: application/json");
	
	//loading xml web page
	$dom = simplexml_load_file($url);
	
	$departures = [];
	
	foreach($dom->xpath("/root") as $root)
	{
		$count = count($root->message->children());
		//check if meassage tag is not empty
		if($count == 0)
		{
			foreach($root->station->etd as $etd)
			{
				$destination = (string)$etd->destination;
				
				foreach($etd->estimate as $estimate)
				{
					$time = (string)$estimate->minutes;
					$platform = (string)$estimate->platform;
					
					//filling the array
					$departures [] = [
						"destination" => $destination,
						"time" => $time,
						"platform" => $platform
						];
				}	
			}	
		}		
	}	
	
	
	//print_r($departures);
	print(json_encode($departures));
?>
