<!DOCTYPE html>
<html>
	<head>
		<title>BART</title>
		<meta name="viewport" content="initial-scale=1.0">
		<meta charset="utf-8">
		<style>
		body,html
		{
			height: 100%;
			width: 100%;
		}
		#map
		{
			margin: 3% auto 3% auto;
			height: 72%;
			width: 85%;
		}
		#panel
		{
			width: 88%;
			margin: 0% 6% 0% 6%;
			height: 10%;
			text-align: center;
			
		}
		.routes
		{
			padding: 0.1%;
			font-family: "Courier New", monospace;
			background-color: blue;
			width: 7%;
			margin: auto 1% auto 0%;
			border-radius: 5px;
			float: left;
			color: cyan;
			text-transform:capitalize;
		}
		.routes:hover
		{
			cursor: pointer;
		}
		table
		{
			font-size: 80%;
			width: 110%;
			border-collapse: collapse;
		}	
		
		th, td, tr
		{
			text-align: center;
		}
		th
		{
			background-color: #0000ff;
			color: #fff;
		}
		td
		{
			background-color: #f2f2f2;
			padding: 1%;
		}
		tr
		{
			border: 1px solid #4d4d4d;
		}
		h1
		{
			color:#9400d3;
			text-transform: uppercase;
			font-family: "Comic Sans MS", "Monotype Corsiva", cursive;
			margin : 0 auto 0 auto;
			text-align: center;
			text-shadow: -5px 2px 2px #d980ff;
		}					
		
				
					
		</style>
		<script async defer src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyADcLXdwQm2h5Cb08bp9rt027ZjpOjpiDc"></script>
		<script>
			var map ;
			
			//function to initialize map
			var initialize = function()
				{
				var mapOptions = {
					center :{lat: 37.775362, lng: -122.417564},
					zoom : 12,
					mapTypeId : google.maps.MapTypeId.ROADMAP
					};
				
				map = new google.maps.Map(document.getElementById('map'),
						mapOptions);
				}
				 						
			
				window.onload = initialize;
				
		</script>
		
	</head>
	<body>
		<h1>bay area rapid transit</h1>
		<div id ="map">
		</div>
		<div id = "panel">
			<div class = "routes" id = "route1">Route 1</div>
			<div class = "routes" id = "route2">route 2</div>
			<div class = "routes" id = "route3">route 3</div>
			<div class = "routes" id = "route4">route 4</div>
			<div class = "routes" id = "route5">route 5</div>
			<div class = "routes" id = "route6">route 6</div>
			<div class = "routes" id = "route7">route 7</div>
			<div class = "routes" id = "route8">route 8</div>
			<div class = "routes" id = "route11">route 11</div>
			<div class = "routes" id = "route12">route 12</div>
			<div class = "routes" id = "route19">route 19</div>
			<div class = "routes" id = "route20">route 20</div>
		</div>
	</body>
	<script type = "text/javascript">
//used for content for info window
var contentstring;

var jsontiming;
// array to store location of every route div
var divlocations = [];

//needed for ajax in function initialize
var xhr = null;

//to store jason
var jason ;

// array to store markers in function addmarkers()
var markers = [];

// to setup polylines in addpolyline function and later used in onclick
var polylinepath;

//to store locations of infowindow in function addinfowindow
var infowindow;

//looping through all the divs and putting a event handler on each
for(var i = 1; i <= 20; i++)
{
	if(i != 9 && i != 10 && i != 13 && i != 14 && i != 15 && i != 16 && i != 17 && i != 18)
	{
		divlocations[i] = document.getElementById('route' + i);
		divlocations[i].onclick = function()
		{
			//getting the id value of clicked div using this that
			//refers to the current element of array the function 
			//is called upon
			var itsid = this.id;
			
			//if markers are already displayed on map then delete it
			if(markers.length > 0)
			{
				var markerssize = markers.length;
				//deleting markers
				for(var i = 0; i < markerssize; i++)
				{
					markers[i].setMap(null);
				}
				
				//empting markers
				markers.length = 0;	
				
				//when markers are set means polyline too set, so removing it
				polylinepath.setMap(null);
				
				//emptying json data from 1st call
				jason.length = 0;
				
			}	
			
			//calling initialzie with route 
			initialize(itsid);
			
			
		}
	}	
}






//function to start ajax call
function initialize(route)
{
	//instantiate xmlhttprequest object
	try
	{
		xhr = new XMLHttpRequest();
	}
	catch (e)
	{
		xhr = new ActivXObject('Microsoft.XMLHTTP');
	}
	
	if(xhr == false)
	{
		alert("ajax not supported on your browser.")
	}
	
	//url for ajax call
	var url = "/mapajax.php?route=" + route;
	
	xhr.onreadystatechange = function ()
	{
		if(xhr.readyState == 4)
		{
			if(xhr.status == 200)
			{
				//do something with jason
				jason = eval("(" + xhr.responseText + ")");
				
				console.log(JSON.stringify(jason));
				//calling function to add markers on this particular route
				addmarkers(jason);
				
				//then calling function to add polylines
				addpolyline(jason);
				
				//then function call to add info windows
				addinfowindow(jason);
								
				
			}
				
		}
	}
	xhr.open("GET", url, true);
	xhr.send(null);	
}	


//function to add markers
function addmarkers(ajaxjson)
{
	// to find the number of stations in the route
	var size = ajaxjson.length;
	
	//looping through each json object and making a marker
	for(var i = 0; i < size; i++)
	{
		markers[i] = new google.maps.Marker({
			position:{lat: parseFloat(ajaxjson[i].latitude), lng: parseFloat(ajaxjson[i].longitude)},
			title: ajaxjson[i].name,
			animation: google.maps.Animation.DROP,
			map: map
		});
	}
		
}


//function to add polylines for routes
function addpolyline(polylineajax)
{
	var polysize = polylineajax.length;
	
	// array to store objects for lat lng
	var polylinecoordinates = [];
	//add to array
	for(var i = 0; i < polysize; i++)
	{
		var a = {lat: parseFloat(polylineajax[i].latitude), lng: parseFloat(polylineajax[i].longitude)};
		//var a = new google.maps.LatLng(parseFloat(polylineajax[i].latitude), parseFloat(polylineajax[i].longitude));
		polylinecoordinates.push(a);
	}

	
	polylinepath = new google.maps.Polyline({
		path: polylinecoordinates,
		strokeColor: polylineajax[polysize - 1].polylinecolor,
		strokeOpacity: 1.0,
		strokeWeight: 2
	});
	
	//setting up polylines
	polylinepath.setMap(map);	
}


//Function to display info window
function addinfowindow(jsoninfo)
{
	//determining the size of markers
	var sizeofmarkers = markers.length;
	
	//default content to display in infowindow if there is no data
	contentstring = "<h5>No Trains data available right now</h5>";
		
	/* looping through 1 less of size of markers cuz last element is
	polylinecolor and adding click listener and displaying infowwindow
	based on closure concept from https://stackoverflow.com/questions/8523993/dynamicaly-adding-infowindows-and-markers-on-google-maps-v3 */
	var previousinfowindow = false;
	for(var i = 0; i < sizeofmarkers; i++)
	{
		markers[i].addListener("click", (function(idx) {
			return function () {
				//closing if any info window was opened
				if(previousinfowindow)
				{
					previousinfowindow.close();
				}	
				
				//calling function timing to get info abt current station
				//and sending the location of clicked marker 
				timings(jsoninfo[idx].stations, idx);
				
				console.log(jsoninfo[idx].stations)
								
				//setTimeout(function () {previousinfowindow = infowindow;}, 2000);
				//settimg this flag so that info window cud b closed if other is clicked
				previousinfowindow = infowindow;
			}
		})(i));
	}	
	
}	


//function that addinfowindow calls for ajax call synchrounusly by making
//station specific call and finding the appropriate marker with 
//value of idx.
function timings(stationcode, idx)
{
	
	var xhr2;
	
	
	try
	{
		xhr2 = new XMLHttpRequest();
	}
	catch (e)
	{
		xhr2 = new ActiveXObject("Microsoft.XMLHTTP");
	}	
	if(xhr2 == false)
	{
		alert("ajax not supported");
	}
	
	var url2 = "/arrivals.php?station=" + stationcode;	
	
	xhr2.onreadystatechange = function () {
		if(xhr2.readyState == 4)
		{
			if(xhr2.status == 200)
			{
				jsontiming = eval("(" + xhr2.responseText + ")");
				console.log(JSON.stringify(jsontiming));
								
				var sizearrivals= jsontiming.length;
					console.log(sizearrivals);
				
				//check if arrivals returned by ajax is not empty is not empty
				if(sizearrivals > 0)
				{
					// setting contentstring to empty ie removing msg "no trains" and make a table
					contentstring = "<table><thead><tr><th  colspan =\"3\">DEPARTURES";
					contentstring += "</th></tr><tr><th>Destination</th><th>Leaves In</th><th>Platform</th></tr></thead>";
					contentstring += "<tbody>";
					
					//looping thru the returned ajax and storing text
					for(var i = 0; i < sizearrivals; i++)
					{
						contentstring += "<tr>";
						//storing ajax data in contentstring
						contentstring += "<td>" + jsontiming[i].destination + "</td>";
						if(jsontiming[i].time == "Leaving")
						{
							contentstring += "<td>" + jsontiming[i].time + "</td>";
						}
						else
						{
							contentstring += "<td>" + jsontiming[i].time + " mins" + "</td>";
						}		
						
						contentstring += "<td>" + jsontiming[i].platform + "</td>";
						contentstring += "</tr>";
					}
					
					//ending the table
					contentstring += "</tbody></table>";
					
					//creating infowindow
					infowindow = new google.maps.InfoWindow({
						content: contentstring,
						position: markers[idx].position});		
				}
				else
				{
					//if empty ajax returns than load window with msg "no train data"
					infowindow = new google.maps.InfoWindow({
						content: contentstring,
						position: markers[idx].position});
				}
				
				//setting up infowindow
				infowindow.open(map, markers[idx]);
							
			}	
		}
							
	}	
	// making this a sycnchronus call buy setting 3rg argument as false 
	// so that the main code only proceeds after finishing this ajax call
	xhr2.open("GET", url2, false);
	xhr2.send(null);
	
		
}	 		
	</script>
</html>
