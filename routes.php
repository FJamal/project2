<?php
	//configuration
	require("includes/config.php");
	
	$url = "https://api.bart.gov/api/route.aspx?cmd=routeinfo&route=all&key=MW9S-E7SL-26DU-VV8V";
	
	//loading the url of bart that has xml
	$dom = simplexml_load_file($url);
	
	//test array
	$test = [];
	
	foreach($dom->xpath("/root/routes/route") as $route)
	{
		// creating table for each route
		$routeid = $route->routeID;
		$routeid = strtolower($routeid);
		
		//sql that will insert the table name
		$sql;
		
		//tablename that would be usefull for the next forrach loop
		$tablename;
		
		// using switch to format the sql for creating appropriate table
		switch ($routeid)
		{
			case "route 1":
				$sql = "CREATE TABLE route1 (
						stations VARCHAR(15)
						)";
				$tablename = "route1";		
				break;
			case "route 2":
				$sql = "CREATE TABLE route2 (
						stations VARCHAR(15)
						)";
				$tablename = "route2";		
				break;
			case "route 3":
				$sql = 	"CREATE TABLE route3 (
						stations VARCHAR(15)
						)";
				$tablename = "route3";								
				break;
			case "route 4":
				$sql = "CREATE TABLE route4 (
						stations VARCHAR(15)
						)";	
				$tablename = "route4";		
				break;
			case "route 5":
				$sql = "CREATE TABLE route5 (
						stations VARCHAR(15)
						)";
				$tablename = "route5";		
				break;
			case "route 6":
				$sql = "CREATE TABLE route6 (
						stations VARCHAR(15)
						)";
				$tablename = "route6";		
				break;
			case "route 7":
				$sql = "CREATE TABLE route7 (
						stations VARCHAR(15)
						)";
				$tablename = "route7";		
				break;
			case "route 8":
				$sql = "CREATE TABLE route8 (
						stations VARCHAR(15)
						)";
				$tablename = "route8";		
				break;
			case "route 11":
				$sql = "CREATE TABLE route11 (
						stations VARCHAR(15)
						)";
				$tablename = "route11";		
				break;
			case "route 12":
				$sql = "CREATE TABLE route12 (
						stations VARCHAR(15)
						)";
				$tablename = "route12";		
				break;
			case "route 19":
				$sql = "CREATE TABLE route19 (
						stations VARCHAR(15)
						)";
				$tablename = "route19";		
				break;
			case "route 20":
				$sql = "CREATE TABLE route20 (
						stations VARCHAR(15)
						)";
				$tablename = "route20";		
				break;
			default:
				$sql = "";
				break;
		}
														
		//executing the statement to create appropriate table
		$dbh->exec($sql);		
		
		//inserting into the created table names of all stations
		foreach($route->config as $station)
		{
			//storing name of each station in this route in an array
			$test = [$station->station];
			
		}	
		
		//determining the number of stations stored in $test
		$size = count($test[0]);
		
		//inserting into tables all stations stored in $test[0]
		for($i = 0; $i < $size; $i++)
		{
			$sql = "INSERT INTO {$tablename} ";
			
			$result = $dbh->prepare($sql . "(stations) VALUES (:station)");
			
			//binding value
			$result->bindValue(":station", $test[0][$i]);
			
			//executing
			$result->execute();
		}	
	}
	//print_r();	
?>
