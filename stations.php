#!/usr/bin/env php
<?php
	/*script to save stations info in a csv file from bart api */
	
	$url = "https://api.bart.gov/api/stn.aspx?cmd=stns&key=MW9S-E7SL-26DU-VV8V";
	
	//loading the website
	$dom = simplexml_load_file($url);
	
	// array to store info for each station
	$stations = [];
	
	foreach($dom->xpath("/root/stations/station") as $station)
	{
		$stations [] = [$station->name,
						$station->abbr,
						$station->gtfs_latitude,
						$station->gtfs_longitude,
						$station->address,
						$station->city,
						$station->county,
						$station->state,
						$station->zipcode];
	}
	
	// storing the array stations in a csv file
	$fp = fopen("stations.csv", "w");
	
	foreach($stations as $station)
	{
		fputcsv($fp, $station);
	}
	
	fclose($fp);		
?>
