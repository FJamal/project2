#!/usr/bin/env php
<?php
	//configuration
	require("includes/config.php");
	
	//checking if command line argument count was
	if($argc != 2)
	{
		print("invalid usage... Usage = ./stationsdb.php path/to/file");
		exit(1);
	}
	
	if(is_readable($argv[1]))
	{
		$file = $argv[1];
		
		//opening file
		$fp = fopen($file, "r");
		
		if($fp == false)
		{
			print("could not open file");
			exit(1);	
		}
		
		//preparing pdo statement
		$sql = $dbh->prepare("INSERT INTO stations (name, abbr, lat,
				longi, address, city, county, state, zip) VALUES 
				(:name,  :abbr, :lat, :long, :address, :city, :county,
				:state, :zip)");
		
		//iterating over each row
		while($row = fgetcsv($fp, ","))
		{
			//print_r($row);
			//binding values
			$sql->bindValue(":name", $row[0]);
			$sql->bindValue(":abbr", $row[1]);
			$sql->bindValue(":lat", $row[2]);
			$sql->bindValue(":long", $row[3]);
			$sql->bindValue(":address", $row[4]);
			$sql->bindValue(":city", $row[5]);
			$sql->bindValue(":county", $row[6]);
			$sql->bindValue(":state", $row[7]);
			$sql->bindValue(":zip", $row[8]);
			$sql->execute();
		}		
	}
	else
	{
		print("could not open file");
	}
	
	fclose($fp);		
			
?>
